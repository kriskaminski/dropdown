/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Layouts,
    affects,
    arraySize,
    definition,
    editor,
    isFilledString,
    pgettext,
    tripetto,
} from "tripetto";
import { Dropdown } from "./";
import { DropdownOption } from "./option";

/** Assets */
import ICON from "../../assets/condition.svg";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    context: Dropdown,
    icon: ICON,
    get label() {
        return pgettext("block:dropdown", "Dropdown option");
    },
})
export class DropdownCondition extends ConditionBlock {
    @affects("#condition")
    @definition("options")
    @affects("#name")
    option: DropdownOption | undefined;

    get name() {
        return (
            (this.option
                ? this.option.name
                : pgettext("block:dropdown", "Nothing selected")) ||
            this.type.label
        );
    }

    get dropdown() {
        return (
            (this.node &&
                this.node.block instanceof Dropdown &&
                this.node.block) ||
            undefined
        );
    }

    @editor
    defineEditor(): void {
        if (this.node && this.dropdown) {
            const options: Forms.IDropdownOption<
                DropdownOption | undefined
            >[] = [];

            this.dropdown.options.each((option: DropdownOption) => {
                if (isFilledString(option.name)) {
                    options.push({
                        label: option.name,
                        value: option,
                    });
                }
            });

            options.push({
                label: pgettext("block:dropdown", "Nothing selected"),
                value: undefined,
            });

            this.editor.form({
                title: this.node.label,
                controls: [
                    new Forms.Dropdown<DropdownOption | undefined>(
                        options,
                        Forms.Dropdown.bind(this, "option", undefined)
                    ),
                ],
            });
        }
    }
}

import {
    Collection,
    Forms,
    definition,
    editor,
    isFilledString,
    name,
    pgettext,
} from "tripetto";
import { Dropdown } from "./";

export class DropdownOption extends Collection.Item<Dropdown> {
    @definition
    @name
    name = "";

    @definition
    value = "";

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:dropdown", "Label"),
            form: {
                title: pgettext("block:dropdown", "Option label"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .autoFocus()
                        .autoSelect(),
                ],
            },
            locked: true,
        });

        this.editor.option({
            name: pgettext("block:dropdown", "Identifier"),
            form: {
                title: pgettext("block:dropdown", "Option identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", "")
                    ),

                    new Forms.Static(
                        pgettext(
                            "block:dropdown",
                            // tslint:disable-next-line:max-line-length
                            "If an option identifier is set, this identifier will be used as selected option value instead of the option label."
                        )
                    ),
                ],
            },
            activated: isFilledString(this.value),
        });
    }
}

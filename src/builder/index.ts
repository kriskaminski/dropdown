/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    editor,
    npgettext,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { DropdownOption } from "./option";
import { DropdownCondition } from "./condition";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:dropdown", "Dropdown");
    },
})
export class Dropdown extends NodeBlock {
    @definition
    @affects("#label")
    options = Collection.of(DropdownOption, this as Dropdown);
    dropdownSlot!: Slots.String;

    get label() {
        return npgettext(
            "block:dropdown",
            "%2 (%1 option)",
            "%2 (%1 options)",
            this.options.count,
            this.type.label
        );
    }

    @slots
    defineSlots(): void {
        this.dropdownSlot = this.slots.static({
            type: Slots.String,
            reference: "option",
            label: pgettext("block:dropdown", "Selected option"),
        });
    }

    @editor
    defineEditor(): void {
        this.editor.groups.general();
        this.editor.name();
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        this.editor.collection({
            collection: this.options,
            title: pgettext("block:dropdown", "Dropdown options"),
            placeholder: pgettext("block:dropdown", "Unnamed option"),
            editable: true,
            sorting: "manual",
        });

        this.editor.groups.options();
        this.editor.required(this.dropdownSlot);
        this.editor.visibility();
        this.editor.alias(this.dropdownSlot);
        this.editor.exportable(this.dropdownSlot);
    }

    @conditions
    defineConditions(): void {
        this.options.each((option: DropdownOption) => {
            if (option.name) {
                this.conditions.template({
                    condition: DropdownCondition,
                    label: option.name,
                    props: {
                        slot: this.dropdownSlot,
                        option: option,
                    },
                });
            }
        });

        this.conditions.template({
            condition: DropdownCondition,
            label: pgettext("block:dropdown", "Nothing selected"),
            props: {
                slot: this.dropdownSlot,
                option: undefined,
            },
        });
    }
}

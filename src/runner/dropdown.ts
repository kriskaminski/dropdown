/** Dependencies */
import {
    NodeBlock,
    Slots,
    arrayItem,
    assert,
    findFirst,
} from "tripetto-runner-foundation";
import { IDropdownOption } from "./option";

export abstract class Dropdown extends NodeBlock<{
    options: IDropdownOption[];
}> {
    /** Contains the dropdown slot. */
    readonly dropdownSlot = assert(
        this.valueOf<string, Slots.String>("option")
    ).confirm();

    /** Contains if the dropdown is required. */
    readonly required = this.dropdownSlot.slot.required || false;

    /** Retrieves the reference of the selection option. */
    get value() {
        const selected = findFirst(
            this.props.options,
            (option: IDropdownOption) =>
                option.id === this.dropdownSlot.reference
        );

        if (!selected && !this.node.placeholder) {
            return this.select(arrayItem(this.props.options, 0));
        }

        return (selected && selected.id) || "";
    }

    /** Sets the reference of the selected option. */
    set value(reference: string) {
        this.select(
            findFirst(
                this.props.options,
                (option: IDropdownOption) => option.id === reference
            )
        );
    }

    /** Selects an option. */
    select(option: IDropdownOption | undefined): string {
        this.dropdownSlot.set(
            option && (option.value || option.name),
            option && option.id
        );

        return (option && option.id) || "";
    }
}

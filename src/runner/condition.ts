/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    condition,
    tripetto,
} from "tripetto-runner-foundation";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class DropdownCondition extends ConditionBlock<{
    option: string | undefined;
}> {
    @condition
    isSelected(): boolean {
        const dropdownSlot = this.valueOf<string>();

        return (
            (dropdownSlot &&
                (dropdownSlot.reference === this.props.option ||
                    (!this.props.option && !dropdownSlot.reference))) ||
            false
        );
    }
}

export interface IDropdownOption {
    /** Id of the option. */
    readonly id: string;

    /** Name of the option. */
    readonly name: string;

    /** Value of the option. */
    readonly value: string;
}
